public class Balls {

   enum Color {green, red};

   public static void main (String[] param) {
      // for debugging
   }

   public static void reorder (Color[] balls) {
      int cntRed = 0; // red ball counter

      // increase Green counter if ball is red
      for (int i = 0; i < balls.length; i++) {
         if (balls[i] == Color.red) {
            cntRed++;
         }
      }

      // return if all balls are red or none are red
      if (cntRed == balls.length) {
         return;
      } else if (cntRed == 0) {
         return;
      }

      // replace the contents of the array with new colors that are in correct order
      for (int i = 0; i < cntRed; i++) {
         balls[i] = Color.red;
      }
      for (int i = cntRed; i < balls.length; i++) {
         balls[i] = Color.green;
      }
   }
}